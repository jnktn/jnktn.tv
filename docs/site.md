# Jnktn.tv Website

Website pages are generated statically with the help of [Hexo](https://hexo.io/), a simple free and open-source static site generator written in [Node.js](https://nodejs.org/en/). Using Hexo makes it possible to reuse many shared components (such as headers and footers) across different pages, maintaining which manually would have been impractical.

## Repository Structure

This repository contains the website content source, website theme, and the recipe for building and testing webpages. All pages are generated from [Markdown](https://www.markdownguide.org/) source files. Markdown is an easy-to-understand and easy-to-write markup language that is highly versatile. It can be easily read and transpiled to other markup languages, such as (X)HTML5. As a result, Markdown has become a popular choice for many blogging systems.

### Content

The content of website pages is located in the [source](../source) directory. It has a folder for [shows](../source/shows/) where all descriptions about the shows at Jnktn should go. Another folder called [team](../source/team) contains a little bit background information about the lovely streamers and team members at Jnktn. These pages are not supposed to follow a strict pattern and are open to modification by individuals. If "streamer A" wants a picture of themselves displayed, it can be done. If "streamer B" (who is privacy-conscious) wants to share no information about themselves, that's also fine.

### Front-Matter

Throughout this guide, you'll eventually come across the term "front-matter". Front-matter refers to meta-data that is embedded as content into the Markdown files. This metadata can be used by the theme to determine how to display certain content. Additionally, it is used to establish links between documents.

#### Front-Matter for a Show

```text
---
title: Dance Attack with Ruffy
streamer: ruffy
show_id: danceattackwithruffy
---
```

##### Front-Matter for a Team member

```text
---
title: Ruffy
streamer: Ruffy
streamer_id: andrina

tags:
    - Streamer

picture: https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg
country: 🇦🇹
---
```

- *title*: Mandatory, displayed as the title on the team-member page
- *streamer*: Mandatory, name of the streamer (only relevant for streamers)
- *streamer_id*: Unique, mandatory, used for linkings in schedules
- *tags* :Optional, what responsibilities/roles a person has
- *picture*: Optional
- *country*: Optional, currently unused

### Custom Plugins

We use some custom, self-made scripts, known as "tag plugins", that allow us to create reusable components within our pages.

#### Mixcloud Embed

The Mixcloud embed plugin embeds an uploaded file from Mixcloud into the Page. The embed works through an `iframe`. It can be used as follows:

Usage in Markdown: `{% mixcloud https://www.mixcloud.com/worky/the-church-of-good-times-w-mrrrkk-ali-tucker-worky/ %}`

The source code for this script can be found [here](../scripts/mixcloud.js).

## Theme

Hexo follows the concept of themes, which strictly separates content (such as displayed text) from the theme (the way the text is represented). The benefit of this approach is that we can switch to another theme at any time in the future without having to manually rewrite all the existing content.

### Templates

```text
/themes/jnktn-main/
    /layout
        /_partial (contains re-usable components, shared across sites/pages)
        online.njk (the template for the Jnktn main page when the stream is online)
        offline.njk (the template for the Jnktn main page when the stream is offline)
        page.njk (the template for literally all other pages)
        /includes
            layout.njk (the main site-layout)
```

Templates are written using [Nunjucks](https://mozilla.github.io/nunjucks/), a templating engine written and maintained by Mozilla.

### Further assets

```text
/themes/jnktn-main/source
    /assets
        /favicons
        /fonts
        /icon
        /img
    /style
        font.css
        index.css
        main.css
        page.css
```

## Setting up the development environment

To modify the files in this repository and install the tools for building and validating pages, you will need to install the following programs on your computer:

- git (a command-line tool for working with Git repositories)
- npm (the package manager for Node.js)

Once you have installed these programs, create a local copy of this repository by running the following command:

```bash
git clone https://codeberg.org/jnktn/jnktn.tv.git
```

That copies the files to `jnktn.tv` directory. Afterwards, navigate to the directory and install the required development tools:

```bash
cd jnktn.tv
npm ci
```

## Building pages

- `npm run build`: Creates a directory called `public` where it stores all the generated static content (the files used by the webservers)
- `npm run server`: Starts a simple webserver, serving the contents, and enabling hot reloading if any changes are made (for development environment only)

## Validation

- `npm run lint-md`: Validates the Markdown files. if you have change any of Markdown files, run this command to ensure your changes follow the standard and best practices.
- `npm run lint-js`: Validates JavaScript files used for building pages.
- `npm run lint-css`: Validates CSS files.
- `npm run lint-schedule`: Validates the [schedule file](../source/_data/schedule.yaml)

## FAQs

### I have the new Schedule for next Saturday. How Can I update it?

Open the [/source/_data/schedule.yaml](../source/_data/schedule.yaml) and modify the data inside.

```yaml
---
Date: "11 June"
slots:
  - begin: "1900"
    end: "2000"
    show_id: danceattackwithruffy
    live: true
  - begin: "2000"
    end: "2130"
    show_id: garyscitrusclub
    live: true
  - begin: "2130"
    end: "2230"
    title: 'Carbon Lifeforms - WTF'
    href: "https://controlfreak.live/"
    live: true

```

- The `date` filed is a textual representation of the schedule date.
- There is a list of slots (schedule entries). Each slot contain the following fields:
  - `begin`: Sets the start-time for a show.
  - `end`: Sets the end-time for a show
  - `show_id`: The unique identifier of the show (find it in the front-matter of the [shows](../source/shows/) directory)
  - `live`: A boolean value (either `true` or `false`) indicating whether this is a live show.
  - `title`: Sets the title of the show, only used when a `show_id` does not exist (e.g., guest show)
  - `href`: Sets the URL of the show, only used when a `show_id` does not exist (e.g., guest show)

---

After saving your changes, validate the new schedule by running the following command. If you have not yet installed the required tools, run `npm ci` to install them first.

```bash
npm run lint-schedule
```

### A new member joined Jnktn - what to do?

A new member joins Jnktn called "Duffy" with a show called "France attack"

1. Create a new page in the [team](../source/team/) directory called `duffy.md`.
1. Fill this page with content provided by the new member using [this template](../source/team/_template.md) as a reference.
1. Create a new page in the [shows](../source/shows/) directory called `franceattack.md`.
1. Fill this page with content provided by the new member using [this template](../source/shows/_template.md) as reference.
1. Re-deploy!

### I want a Mixcloud recording of a streamer to be visible on the streamers page

In case a streamer wants to highlight a set (e.g. a very special moment, or just a set that was 200% fire), this can be done with a Mixcloud embed. Simply add `{% mixcloud link %}` to the page where you want the embed to be displayed.

### I want to change my own streamer page

1. `git clone https://codeberg.org/jnktn/jnktn.tv`
1. `cd jnktn.tv`
1. `npm ci`
1. `git checkout -b duffy/adaptMyStreamerPage`
1. find the corresponding page under `source/team`
1. edit it to your likings
1. preview it by running `npm run server` (spawns a simple webserver on `localhost:4000`)
1. open up your favorite browser and navigate to [localhost:4000](http://localhost:4000)
1. navigate to the page you have edited
1. verify that it looks good to you
1. `git commit -m "a descriptive text of what you have changed and why"`
1. `git push -u origin duffy/adaptMyStreamerPage`
1. send a Jnktn repository maintainer a Merge-Request (or pull-request)

---

### I'm working on the Theme and hexo does not pick up changes

`npm run server` generates the contents once it's started, and caches it in a file called `db.json`. This makes the browsing experience on a local machine very comfortable. However sometimes "db.json" can get out of sync, and some changes you made might not be picked up correctly. This happens when you work on tag plugins. If something like this happens, simply call `npm run clean` and it'll wipe the `db.json` clean.

### Checking external links in CI is failing

Some websites may have blacklisted the IP address of GitLab runners, which run the test jobs. Most of them will return an `HTTP 403-Forbidden` response. If you have locally verified that the link is working from your own machine, you can safely ignore this error.
