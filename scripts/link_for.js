'use strict';

hexo.extend.tag.register('link_for', (args, _) => {
  const url = args[0];
  const text = args[1];
  const urlOffset = hexo.config.root || '/';
  return `<a href="${urlOffset}${url}">${text}</a>`.replace('//', '/');
});
