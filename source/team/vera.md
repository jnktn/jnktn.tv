---
streamer: Vera
streamer_id: vera
title: Vera
picture: none
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Streamer
---

I love jalapeño.
<!-- more -->

## Vera

| Facts        |              |
|:------------:|:------------:|
| Country      | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns     | she/her      |
| Joined Jnktn | 2020         |

### Intro

Originally from Greece, I fell in love with Scottish people and Scotland while I was in Glasgow for 6 months for uni. Fast forward and 12 years later I now am based in the capital of Scotland, Edinburgh and enjoy all aspects this city has to offer.

My music taste was always varied, from classical to metal and beyond. Being involved with the Jnktn.tv project helped me broaden those music horizons, now I can tolerate or even enjoy music that in the past I would consider noise – Thank you Jnktn.tv!!!

A few years ago Vera’s Radio Show was born, a vibrant and loud – most of the times, show that has the host, myself, talk and try to have your bumcheeks shaken. #ShakeYourBumCheeks is the catchphrase of the show!

### Favourite artist

a lot are up there BUT for those who know me you know that you cannot listen Sean Paul and not Shake your bumcheeks, so have to go with DA man.

### Favourite gig

The man has some energy, we almost put the stadium on fire from dancing – yes you guessed it is Sean Paul himself a few years ago in Glasgow.

### Favourite drink

Bubbles - no no the one you think, just the one with no alcohol and just pure water.

### Favourite jnktn.tv moment

There are quite a few to pick from, but one that stack in my mind has to be when my full show was done without the mic to be connected – the mix turned out ok, but always checking if mic is connected now!

### Aspirations

One day I will learn how to program, this day is coming closer and closer...
