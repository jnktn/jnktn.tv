---
streamer: JC
streamer_id: jc
title: JC
picture: none
country: 🏴󠁧󠁢󠁳󠁣󠁴󠁿
tags:
    - Tech Support
    - DJ
---

Not famous enough for a good bio. Sells juice on the side.

<!-- more -->

## JC

| Facts          |              |
|:--------------:|:------------:|
| Country        | Scotland 🏴󠁧󠁢󠁳󠁣󠁴󠁿  |
| Pronouns       | he/him       |
| Joined Jnktn   | 2020         |
| Dogs Vs. Cats  | ![catjam](../assets/img/memes/catjam.gif) |
| Advice for you | Go water your houseplant |

### Favourite genres

- Classic 90s cheese
- electronic
- techno
- trance
- Bit of reggae thrown in for good measure

### About

At some point in life people get in touch with electronic dance music for the first time. To some this moment is as important as being born. For JC the gateway drug to that world was [Daft Punk](https://www.daftpunk.com/) and [Tiësto](https://www.tiesto.com/). Aside from raving 24 hours 7 days per week JC has three major hobbies, beginning with breakfast, continuing with lunch and ending the day with something really unexpected: dinner.

### My favourite Jnktn moments

- [Jumboshrimp](jumboshrimp) indulging us with a bit of light household cleaning
- [Gabe](https://gabekangas.com/)'s debut on Jnktn
- bringing home the jnktn xmas bonanza with Celine Dion
