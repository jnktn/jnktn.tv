---
streamer: Michan
streamer_id: michan
title: Michan
tags:
    - Streamer
    - Tech
---

An illusion of counciousnes.

<!-- more -->

## Michan

| Facts        |           |
|:------------:|:---------:|
| Pronouns     | he/it     |
| Joined Jnktn | 2020      |

### Who?

Hi there! My username is Michan in the chat. I'm a [high functioning] music addict.

### Why music?

I'm a Polyglot and for me music is yet another language. A language for my inner voice with its own unique words and concepts which are not easy to explain in other languages.

### What am I doing?

Each month I add a pinch of [my favourite spice mix](../shows/unpopularstream) to all the cool tunes people share on jnktn.tv. Also, from time to time I contribute to jnktn.tv's infrastructure as well as the opensource tools used around here.

### Why here?

I understand that the context is crucial and spending a lot of time in weird places to find an obscure piece of music and listening to numerous uninspiring tunes in the process is an important part of the ritual for meeting a special one. But I also believe that there are people with curious imagination who can hear through all the eccentricity and appreciate the beauty to a great extent. Jnktn.tv is full of these people. A group of friends from different backgrounds who meet at least once a week right here!

### What's on my ears?

If I'm intently listening to something:

* Beyerdynamic DT1990 Pro (with SMSL SH-9, SU-9)
* Etymotic ER2SE (sometimes with Audioquest DragonFly Black)
* Audio Technica ATH-M50x

If it's supposed to be mixed in the background of my life [regrettably!]:

* Sennheiser Momentum 4 Wireless
* KZ ZS10 Pro

### What if I can't use headphones or IEMs?

Most probably it's in the shower and something is playing in the background on:

* Anker SoundCore 2

### What's playing?

From shitgaze to witch house, I listen to music in every genre. But, the most fascinating ones for me are cross-genre remix/flips, and those which don't follow the beaten path.

### Which Rammstein perfume?

All of them depending on my mood - Yes! Even the *Seemann* - But I think I'm reaching more often for the *Kokain Black Intense* in cold and for the *Kokain Gold* in warm days!

### Wh... [more questions]?

Drop me an email: "michan" [AT] whatever the name of this site was!
