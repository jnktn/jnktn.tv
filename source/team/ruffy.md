---
streamer: Ruffy
streamer_id: ruffy
title: Ruffy
picture: https://files.mastodon.social/accounts/avatars/000/394/214/original/667b3703d6b69d11.jpg
country: 🇦🇹
tags:
    - Streamer
    - DJ
---

This one is dedicated to all the ravers in the nation.

<!-- more -->

## Ruffy

| Facts        |             |
|:------------:|:-----------:|
| From         | Linz, AT 🇦🇹 |
| Pronouns     | he/him      |
| Joined Jnktn | 2021        |
| Cats/Dogs    | Woof Woof   |

### About me

Hey stranger, happy you managed to reach my Jnktn page. My name is ruffy (pronounced like: "roughy") and I'm a music-producing software-developer. It's like you'd put a programmer and a DJ into a blender, blend it for like 20 minutes and whatever comes out, is most likely called ruffy. Also a huge fan, supporter and enthusiast of F(L)OSS projects and pretty much all kinds of technology. Aside from all this tech stuff, I'm producing music, mostly electronic dance music. Here on Jnktn I'm a resident DJ with a somewhat regular show called [Dance Attack](../shows/danceattackwithruffy). Sometimes I help out a little bit behind the scenes.

### Latest work

{% soundcloud track 1221158164 %}
{% soundcloud track 1098710812 %}
{% soundcloud track 1099278472 %}

### Get in touch

- [Mastodon](https://mastodon.social/web/@rarepublic)
- [Ruffys Blog](https://blog.rtrace.io)
- [Soundcloud](https://soundcloud.com/djraremusic)
