---
title: Jumboshrimp's catch of the week
streamer_id: jumboshrimp
show_id: jumboshrimpscatchoftheweek
---

If you're into quality and variety, look no further, since Jumboshrimp will make sure to sort you out with a feast for the ears.

<!-- more -->

## Jumboshrimps' Catch of the week

### Hosted by: {% link_for '/team/jumboshrimp' Jumboshrimp %}

Tune in as jumboshrimp brings ashore his picks of the week. If you're into quality and variety, look no further, since he'll make sure to sort you out with a feast for the ears. Whether he sticks to a genre or mixes it up, it's always set to be a adventure. What will be his 'tuna the day'? Only one way to find out!

### What's being played?

- Trance
- Garage
- Future
- Rock & Pop
- House & Electro
