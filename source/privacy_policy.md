---
title: Privacy Policy
---

## Privacy Policy

Respecting users' privacy is one of our core principles. We, here at Jnktn, take your privacy very seriously. In general we're **NOT** interested in collecting any information about you or what you do online. This page explains what kind information can be exposed through the use of third-party software and services to their servers. We may occasionally update this page. Users must check the latest revision before using the website or other related services. Important changes to this page are listed in the [Revision history](#revision-history) section below. For a detailed history of changes to this page checkout the [git history of its content](https://codeberg.org/jnktn/jnktn.tv/commits/branch/mistress/source/privacy_policy.md).

For general recommendations regarding privacy, checkout [Jnktn’s guide to online privacy](./privacy_guide).

### Mandatory Services

We consider the main functionality of Jnktn.TV to be the Video Stream and the possibility to chat with other people in the community. The following services are required to fullfill this functionality.

#### [Owncast](https://stream.jnktn.tv)

Owncast hosted on [stream.jnktn.tv](https://stream.jnktn.tv) is embedded as an `iframe` to [Jnktn.TV](https://jnktn.tv). Owncast does not store any cookies.

Owncast chat uses your browsers' Local-Storage to persist settings and information for its chat functionality. No personal information is reflected on server-side to track users and/or their behaviors. Stored information include:

- `owncast_access_token` used as session-token
- `owncast_first_message_sent` indicator whether you already sent a message
- `owncast_username` the chat-username assigned to you (or overridden by you)

### Optional Services

Parts of our website utilize third-party services with their own privacy statements. We consider these parts of Jnktn as "optional". Those sites are not needed for our primary purpose.

#### [Mixcloud](https://mixcloud.com)

Mixcloud is a audio-streaming platform used by Jnktn to save recordings of our shows. Mixcloud embeds are embedded throughout the [team member pages](./team/) and the [shows pages](./shows/). Please read about [Mixclouds Privacy Policy](https://www.mixcloud.com/privacy/) for further details.

#### [Soundcloud](https://soundcloud.com)

Soundcloud is a audio-streaming platform that allows embedding tracks and playlists on to our website. Soundcloud embeds are used throughout the [team member pages](./team/) and the [shows pages](./shows/). Please read about [Soundcloud Privacy Policy](https://soundcloud.com/pages/privacy) for further details.

### Revision history

- 02.11.2024: add owncast chat, update the links
- 11.10.2022: initial public release
