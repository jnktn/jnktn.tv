---
title: A fresh start!
author: michan
date: 2024-10-12
modified: 2024-10-27
excerpt: "We're back baby!"
---

## What happened?

As you may already know, offline responsibilities and family obligations of a key contributor to the jnktn.tv project ({% link_for '/team/jumboshrimp' Jumboshrimp %}) forced us to stop our regular streams. Our infrastructure had grown considerably over the past few years in size, cost and complexity, it was relying on a dedicated VPS, without an automatic way to configure it. Most of our infrastructure had been set up and managed by Jumboshrimp himself.

At that time, we reached out to the community for help with maintaining our infrastructure, managing our social media presence, and scheduling the streams. However, due to the hands-on nature of our workflow, lack of infrastructure documentation, and very limited number of volunteers with the required expertise, this transition was unsuccessful. A couple of volunteers offered help if we were willing to use Amazon or Google services to simplify their tasks. Despite this being true, as it goes directly against our core principles, we declined their offer.

Scheduling streams, communication, and monitoring of services needed to be done on a weekly basis and we could not find a volunteer (or a group) willing to commit to this kind of regular schedule.

Jumboshrimp asked me to take over managing the project in his absence but due to some unforeseen health issues, I did not have much productive time to spend on jnktn until now.

## What did we learn?

Simplicity and documentation of infrastructure is a must for financial sustainability, and maintainer-independence of a project of this size and complexity. We have grown out of a hobby project into a community that a lot of people depend on and we have to adapt a professional approach to provide reliable services for them.

## What is being done and what are our plans?

We are currently redesigning and rebuilding our infrastructure from the ground up, trying to simplify it, secure it, and run it partly on the machines which we own ourselves. Our effort is focused on documentation of whole infrastructure from creating the VMs, to configuring and removing them using scripts. We are also developing tools to minimize the required volunteer time for managing and maintaining our services including:

- Web-based scheduling application
- Stream switching tool
- Automatic posting to Fediverse

We are also setting up a mailing list to centralize our communication channels for streamers.

Our plan is to start regular streams soon. Initially it will be a monthly stream as some of our efforts will be dedicated to infrastructure works but we should return to our usual weekly streaming schedule soon after.

## Final words

I want to personally thank all the contributors of the projects and the fans who were with us all the way up to this point. I hope jnktn.tv will continue being a home for all of us.
