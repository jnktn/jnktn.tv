---
title: About Us
---

## About Us

Based in Edinburgh, Scotland, Jnktn.tv is a streaming platform that makes use of and promotes open-source software. We host an eclectic mix of music-based programming from an international family of curators and broadcast them to a global audience every Saturday. [Our core principles page](core_principles) summarizes the main ideas which brings us up all together.

### Humble beginnings

Started by a small group of friends in July 2020, the idea was to create an online space to connect those affected by the widespread lockdowns of the time. With the hospitality industry closed down indefinitely, our aimwas to help engage, entertain and provide respite from the stress of pandemic life. We also wanted to give people a good reason to be dancing around the living room in their pajamas!

### Where we're at

Now in its second year of operation, Jnktn.tv has gone from strength to strength. Over this time, despite losing important members of the initial Jnktn team, we have welcomed others onboard, expanded our residency programming, provided room for new shows and have increased our viewership along the way. We've built a community of people from different parts of the world and walks of life and are always keen to extend the Jnktn family. We are grateful to all the people who helped us reach this point including our [current team members](team/) who are actively contributing to the project and our [guests](guests) who motivated us to continue this journey.

### The future

With the end of the pandemic now in sight, we aim to continue providing entertainment from our corner of the internet as life returns and readapts to normality. Why not join us next time we're live to see what we're all about? You can find our schedule on the homepage.

### Community Outreach

To promote the use of open source software, we have [published](https://codeberg.org/jnktn) the Jnktn.tv website, its documentation, our infrastructure, and all the tools developed for it under free and open licenses. We are also contributing to the development of software we use by reporting issues, implementing new features as well as bug fixes and monetary donations. We are also regularly adding information on the artists featured on jnktn.tv to open encyclopedias and databases such as [MusicBrainz](https://musicbrainz.org) and [Wikidata](https://www.wikidata.org).

### Get involved!

Fancy yourself a slot on our platform? Have ideas, suggestions or feedback? Want to know how you can help out? Get in contact! We'd love to hear from you!

<h3 class="link"><a href="contact">Get in contact</a></h3>
