## Jnktn.tv

Welcome to the Jnktn.tv repository. Here you'll find the code for our website. Jnktn.tv is powered by the [Owncast](https://github.com/owncast/owncast) and [Cbox Live Chat](https://www.cbox.ws).

If you'd like to help out with the development, PRs are welcome.
